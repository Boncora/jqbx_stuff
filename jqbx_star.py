from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import bs4
import time
import re
import spotipy
import spotipy.util as util
from pathlib import Path
import os
import random
from datetime import datetime
from datetime import date
from datetime import timedelta
import csv
import requests
import pandas as pd
from googleapiclient.discovery import build

def youtube_search(song, artist, browser):
    print(f'{song} {artist}')
    api_key = os.environ.get('google_youtube_api_key')
    print(api_key)
    youtube = build('youtube', 'v3', developerKey=api_key)
    request = youtube.search().list(
            part='snippet',
            q=f'{song} {artist}'
            )
    response = request.execute()
    print(response['pageInfo']['totalResults'])
    print(response['items'][0]['id'])
    if response['pageInfo']['totalResults'] == 0:
        send_message(browser, f"I can't find anything for this song.")
    else:
        send_message(browser, f"https://www.youtube.com/watch?v={response['items'][0]['id']['videoId']}")

def high_score(browser, number_of_hours):
    headers = ['user', 'spotify_id', 'song', 'artist', 'starred_by_user', 'starred_by_id', 'total_stars', 'total_dopes', 'total_nopes', 'date']
    df = pd.read_csv('rock_song_history.csv', names=headers)
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d %H:%M:%S')
    now = datetime.now()
    yesterday_match = (df['date'] > (now - timedelta(hours=number_of_hours))) & (df['date'] < now)
    yesterday_spins = df.loc[yesterday_match]
    unique_users = yesterday_spins['spotify_id'].unique()
    score_list = []
    yesterday_high_score = {k:0 for k in unique_users}
    for user in unique_users:
        user_match = yesterday_spins['spotify_id'] == user
        for i in yesterday_spins[user_match]['total_stars']:
            yesterday_high_score[user] += i
        username = yesterday_spins[user_match]['user'].iloc[0]
        score_list.append((yesterday_high_score[user], username))
    sorted_list = sorted(score_list, key=lambda tup: tup[0], reverse=True)
    send_message(browser, f"Star high scores for last {number_of_hours} hours:")
    for i in sorted_list[:5]:
        send_message(browser, f"{i[1]} earned {i[0]} stars")

def jqbx_login(room):
    '''
    Opens selenium browser and log into the Pizza&Beer JQBX room.
    returns the selenium instance of the browser for other functions to interact with.
    '''
    spotify_user = os.environ.get('spotify_bot_username')
    spotify_password = os.environ.get('spotify_password')
    browser = webdriver.Firefox()
    browser.get(f'http://app.jqbx.fm/room/{room}')
    spotifybutton = browser.find_element_by_css_selector('.btn')
    spotifybutton.click()
    browser.find_element_by_css_selector('#login-username').send_keys(spotify_user)
    browser.find_element_by_css_selector('#login-password').send_keys(spotify_password)
    browser.find_element_by_css_selector('#login-button').click()
    if room == '4d3d3d3':
        time.sleep(15) #gives me a second to enter room password when I'm testing something.
        return browser
    time.sleep(5)
    return browser

def last_song_write(last_song, room):
    columns = [k for k,v in last_song.items()]
    outputFile = open(f'{room}_song_history.csv', 'a', newline='', encoding = 'utf-8-sig')
    outputWriter = csv.DictWriter(outputFile, fieldnames=columns)
    outputWriter.writerow(last_song)
    outputFile.close()



admins = [  '1236359073',
            'glennsavage',
            'taylorsether',
            'mlarsonmail',
            'highlydope',
            'aleesameesa',
            'danielleak',
            'shanegriffin13',
            'slave2magellan',
            'scirton',
            '12151118838',
            'keithkenez',
            'joemoreno19',
            '12120948175',
            '22l7eiulblpagjiipeenhgwvy',
            'acslater1969',
            'dark0wl',
            'joelehansen',
            '22hd2cau5hgxn2fzsgik4m2dy',
            'bdygert5150',
            'dengar69',
            'bolan33',
            '22hwactq6hwekdffygskkcsda',
            'boncora',
            'loubat99',
            '1271887440',
            '1211475227',
            'rappscallion',
            'jbsulliv81',
            'docnight',
            'adokis',
            'jaredwieseler',
            '124560766',
            'roomx',
            'tommy18anderson',
            ]



def progress_bar(browser):
    progress = browser.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/div[2]/div[1]/div[2]/div/div/div')
    return int(progress.get_attribute('style')[6:9].strip(' .%'))


def get_listeners(browser):
    browser.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/div[3]/div[2]/ul[1]/li[1]/a').click() #clicks listener tab
    listeners = browser.find_elements_by_xpath('/html/body/div[1]/div/div/div/div[1]/div[3]/div[2]/ul[2]/li[1]/div/div/div/div/div/div[1]/div/li')
    room_listeners = []
    for i in listeners:
        username = i.find_element_by_xpath('div/div[2]/a').text
        status = i.find_element_by_xpath('div/div[1]/div').get_attribute('class')
        listener = {'spotify_id': i.get_attribute('data-id'), 'user': username, 'doped': False, 'starred': False, 'noped': False}
        if 'thumbs-up' in status:
            listener['doped'] = True
        if 'star' in status:
            listener['starred'] = True
        if 'thumbs-down' in status:
            listener['noped'] = True
        room_listeners.append(listener)
    return room_listeners


def battle_vote():
    pass
def strawpoll_create(choices, title):
    api_key = os.environ.get('STRAWPOLL_API_KEY')
    data = {
    	"poll": {
    		"title": title,
    		"answers": choices,
    		# "description": "Description Text",
    		"priv": 0,
    		# "ma": 0,
    		# "mip": 0,
    		# "co": 1,
    		# "vpn": 0,
    		"enter_name": 1,
    		# "has_deadline": 1,
    		# "deadline": "2020-02-27T07:00:00.000Z",
    		# "only_reg": 0,
    		# "has_image": 0,
    		# "image": None,
    	}
    }
    poll = requests.post("https://strawpoll.com/api/poll", json=data, headers={'API-KEY': api_key}).json()
    return(poll["content_id"])

def strawpoll_results(poll):
    poll = requests.get(f'https://strawpoll.com/api/poll/{poll}')
    print(poll)
    return poll




def message_tracker_with_whispers(browser):
    '''
    Parses the rooms chat history window.
    returns a dictionary with the JQBX username (str), the message (str), and spotify ID (str) of the user that sent the message.
    '''
    message_regex = re.compile(r'(?:<p class="content">(?:(?:.*?"mention">.*?</span>)?(.*?))</p>)')
    #message_regex = re.compile(r'(?:<p class="content">(?:(?:.*?"mention">.*?</span>)?(?:.*?img.*?src="(http.*gif)")?(.*?))</p>)')
    last_user_regex = re.compile(r'class="message\s+(?:private)?\s*named.*?spotify:user:(.*?)">(.*?)<')
    chat_window_html = browser.find_element_by_css_selector('#chat-messages').get_attribute('innerHTML')
    chat_window_html = browser.find_element_by_css_selector('#chat-messages').get_attribute('innerHTML')
    soup = bs4.BeautifulSoup(chat_window_html)
    all_messages = re.findall(message_regex, str(soup))
    user_check = re.findall(last_user_regex, str(soup))
    #[-1] grabs the last message and user found since we're only interested in the most recent message.
    last_message = {'user': user_check[-1][1],
                    'text': all_messages[-1],
                    'spotify_id': user_check[-1][0]}
    return last_message, all_messages


def get_queue(browser):
    dj_queue = browser.find_elements_by_xpath('//*[@id="root"]/div/div/div/div[1]/div[2]/div[2]/div/div[1]/li')
    dj_ids = [i.get_attribute('data-id') for i in dj_queue]
    dj_status = [i.get_attribute('class') for i in dj_queue]
    dj_name = [i.get_attribute('title') for i in dj_queue]
    queue = [{'spotify_id': dj_ids[i], 'user': dj_name[i], 'status': dj_status[i].strip()} for i in range(len(dj_queue))]
    return queue



def slots_history_write(history):
    outputFile = open('slots_history.csv', 'w', newline='')
    outputWriter = csv.writer(outputFile)
    for i in history:
        outputWriter.writerow([i[0], i[1]])
    outputFile.close()

def slots_history():
    open_file = open('slots_history.csv', encoding = 'cp850')
    reader = csv.reader(open_file)
    slots_history = list(reader)

    open_file = open('gamblers.csv', encoding = 'cp850')
    reader = csv.reader(open_file)
    gamblers = list(reader)
    gamblers = [i[0] for i in gamblers]

    return slots_history, gamblers




def slots(message, browser):
    now = datetime.now()
    history, gamblers = slots_history()
    queue = get_queue(browser)
    slots_user = [message['spotify_id'], now.strftime('%Y-%m-%d %H:%M:%S')]
    history = [i for i in history if int((now- datetime.strptime(i[1], '%Y-%m-%d %H:%M:%S')).total_seconds() / 60) < 45 ]
    active_djs = [i['spotify_id'] for i in queue]
    if message['spotify_id'] not in active_djs:
        send_message(browser, f"You've gotta be in the queue to play slots! Step up and DJ @{message['user']}")
        return
    if message['spotify_id'] not in gamblers:
        send_message(browser, f':pizza::pizza::pizza:')
        send_message(browser, f'JACKPOT! Wow! Beginners luck I guess? Get to the front of the queue!')
        history.append(slots_user)
        slots_history_write(history)
        outputFile = open('gamblers.csv', 'a', newline='')
        outputWriter = csv.writer(outputFile)
        outputWriter.writerow([message['spotify_id']])
        outputFile.close()
        return
    for user in history:
        if slots_user[0] in user[0]:
            time_remaining = 45 - int((now- datetime.strptime(user[1], '%Y-%m-%d %H:%M:%S')).total_seconds() / 60)
            send_message(browser, f"You're all outta luck kid. You can only play once every 45 minutes. Try again in {time_remaining} minutes")
            return
    history.append(slots_user)
    slots_history_write(history)
    if queue[-1]['spotify_id'] == message['spotify_id']:
        send_message(browser, f'Slots from the back of the queue? No risk no reward! You get to spin the wheel of death.')
        reel1 = [':bomb:', ':bomb:', ':bomb:', ':bomb:', ':pizza:', ':bomb:', ':beer:', ':bomb:', ':bomb:']
        reel2 = [':bomb:', ':bomb:', ':bomb:', ':bomb:', ':pizza:', ':bomb:', ':beer:', ':bomb:', ':bomb:']
        reel3 = [':bomb:', ':bomb:', ':bomb:', ':bomb:', ':pizza:', ':bomb:', ':beer:', ':bomb:', ':bomb:']
        result1 = reel1[random.randrange(len(reel1))]
        result2 = reel2[random.randrange(len(reel2))]
        result3 = reel3[random.randrange(len(reel3))]
        results = [result1, result2, result3]
        send_message(browser, f"{''.join(results)}")
        if results.count(':bomb:') >= 2:
            send_message(browser, f'Tough luck {message["user"]}... To the end of the queue you go. Oh wait...')
        elif results.count(':pizza:') == 3 or results.count(':beer:') == 3:
            send_message(browser, f"Wow. Really lucky spin {message['user']}. Front of the queue...")
        elif results.count(':pizza:') == 1 and results.count(':beer:') == 1:
            return
        else:
            send_message(browser, f"Thats surprising. {message['user']} can jump up 2 slots.")
        return

    reel1 = [':bomb:', ':boom:', ':hamburger:', ':fries:', ':pizza:', ':icecream:', ':beer:', ':snake:', ':see_no_evil:']
    reel2 = [':bomb:', ':boom:', ':hamburger:', ':fries:', ':pizza:', ':icecream:', ':beer:', ':snake:', ':hear_no_evil:']
    reel3 = [':bomb:', ':boom:', ':hamburger:', ':fries:', ':pizza:', ':icecream:', ':beer:', ':snake:', ':speak_no_evil:']
    result1 = reel1[random.randrange(len(reel1))]
    result2 = reel2[random.randrange(len(reel2))]
    result3 = reel3[random.randrange(len(reel3))]
    results = [result1, result2, result3]
    send_message(browser, f"{''.join(results)}")
    if results.count(':bomb:') == 2 or results.count(':boom:') == 2 or results.count(':snake:') == 2:
        send_message(browser, f'{message["user"]} loses! Move back 2 positions in the queue!')
    elif results.count(':bomb:') == 3 or results.count(':boom:') == 3:
        send_message(browser, f'{message["user"]} WOMP WOMP! GET TO THE END OF THE QUEUE!')
    elif results.count(result1) == 2 or results.count(result2) == 2:
        send_message(browser, f'Congrats {message["user"]}. An admin/mod can move you up 2 spots in the queue')
    elif results.count(':see_no_evil:') == 1 and results.count(':hear_no_evil:') == 1 and results.count(':speak_no_evil:') == 1:
        send_message(browser, f'hmmm.. {message["user"]}.I dunno what to do with this combo yet. but its cool.')
    elif results.count(result1) == 3:
        send_message(browser, f"CONGRATULATIONS {message['user'].upper()}!!!! YOU WIN A TRIP TO THE FRONT OF THE QUEUE! https://media.giphy.com/media/3orif9DJNKDfXqXbBS/giphy.gif")


'''
            if voting == False and message['text'].lower()[:12] == '/battle-vote' and '9lmdbr25slw401t2mmq86y2sd' not in message['spotify_id']:
                voting = True
                results = {}
                history = get_jqbx_history(browser, room, False)
                number_of_entries = int(message['text'].lower()[12:].strip())
                nominees = []
                for i in range(number_of_entries):
                    print(history[i])
                    print(history[i][1])
                    nominees.append(f'{history[i][5]} -- {history[i][3]} -- {history[i][4]}')
                nominees = [i.strip() for i in nominees]
                poll = strawpoll_create(nominees, f"BATTLE!!!")
                send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')

            if voting == False and message['text'].lower()[:11] == '/vote start' and '9lmdbr25slw401t2mmq86y2sd' not in message['spotify_id']:
                voting = True
                results = {}
                nominees = message['text'][12:].lower().split(',')
                nominees = [i.strip() for i in nominees]
                poll = strawpoll_create(nominees, 'JQBX Pizza/Beer poll')
                send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')
                #ballots = {}
                #send_message(browser, f'VOTE FOR {"/".join(nominees)}!!!')
                #send_message(browser, f'p/b admins can cancel the voting with /vote-reset')
                #send_message(browser, f'doop-bot will /w reply if your vote is successful. Polls close at the end of the current song.')
                #send_message(browser, f'copy/paste one of the below /w commands to submit your vote!')
                #for i in nominees:
                #    send_message(browser, f':arrow_right:/w @doop-bot vote-for {i}')
            if message['text'].lower() == '/slots':
                slots(message, browser)
            if message['text'].lower() == '/vote-reset' and '9lmdbr25slw401t2mmq86y2sd' not in message['spotify_id'] and message['spotify_id'] in admins:
                voting = False
                nominees = []
'''
def voting(command, vote_style, voting):
    if voting == False and vote_style == 'battle':
        voting = True
    

def message_tracker(browser):
    '''
    Parses the rooms chat history window.
    returns a dictionary with the JQBX username (str), the message (str), and spotify ID (str) of the user that sent the message.
    '''
    message_regex = re.compile(r'<p class="content">(.*?)</p>')
    last_user_regex = re.compile(r'class="message\s+named.*?spotify:user:(.*?)">(.*?)<')
    chat_window_html = browser.find_element_by_css_selector('#chat-messages').get_attribute('innerHTML')
    chat_window_html = browser.find_element_by_css_selector('#chat-messages').get_attribute('innerHTML')
    soup = bs4.BeautifulSoup(chat_window_html)
    all_messages = re.findall(message_regex, str(soup))
    user_check = re.findall(last_user_regex, str(soup))
    #[-1] grabs the last message and user found since we're only interested in the most recent message.
    last_message = {'user': user_check[-1][1],
                    'text': all_messages[-1],
                    'spotify_id': user_check[-1][0]}
    return last_message

def fourtwenty_spin():
    '''
    d-loc thought it would be neat if we had a daily 4:20 stoner track.
    JQBX users that lived in a specific time-zone could skip the line at their local time 4:20pm
    as long as the song they chose was some like bob marley, willie nelson, snoop dogg, etc.. you get the idea.

    This posts a friendly reminder to the room chat when that time is approaching. 
    '''
    now = datetime.now()
    eastern = now.replace(hour=15, minute=20, second=0)
    central = now.replace(hour=16, minute=20, second=0)
    mountain = now.replace(hour=17, minute=20, second=0)
    pacific = now.replace(hour=18, minute=20, second=0)
    seconds_to_eastern = (eastern - now).seconds
    seconds_to_central = (central - now).seconds
    seconds_to_mountain = (mountain - now).seconds
    seconds_to_pacific = (pacific - now).seconds
    if seconds_to_eastern == 420:
        time.sleep(1)
        message = 'https://media.giphy.com/media/Hy9FU7i9TEQcU/giphy.gif Attention stoners, the eastern timezone 420 play is coming up in approximately, wait, what are we talking about?'
        return message
    elif seconds_to_central == 420:
        time.sleep(1)
        message = 'https://media.giphy.com/media/l2Jeb3ANx8uQY3G2Q/giphy.gif Attention stoners, the central timezone 420 play is coming up in approximately, wait, what are we talking about?'
        return message
    elif seconds_to_mountain == 420:
        time.sleep(1)
        message = 'https://media.giphy.com/media/l0HlP5jWvUrH66kla/giphy.gif Attention stoners, the mountain timezone 420 play is coming up in approximately, wait, what are we talking about?'
        return message
    elif seconds_to_pacific == 420:
        time.sleep(1)
        message = 'https://media.giphy.com/media/fpjdKdw0YwjHa/giphy.gif Attention stoners, the pacific timezone 420 play is coming up in approximately, wait, what are we talking about?'
        return message
    else:
        message = 'None'
        return message

def now_playing(browser):
    '''
    Parses the song title and artists of the current song playing in the room.
    returns song title (str), and a for the artist name (str) (comma separated if multiple artists)
    '''
    last_user_regex = re.compile(r'class="message\s+named.*?spotify:user:(.*?)">(.*?)<')
    now_playing_regex = re.compile(r'_blank">(.*?)</a>')
    now_playing_section = browser.find_element_by_css_selector('#root > div > div > div > div.container > div.center-tabs > div.music > div.top > div:nth-child(2) > div > div > div.copy > div > h4').get_attribute('innerHTML')
    now_playing_artist_section = browser.find_elements_by_xpath('//*[@id="root"]/div/div/div/div[1]/div[2]/div[1]/div[1]/div[2]/div/div/div[2]/div/ul')
    #song titles are always a single value, but sometimes a list of artists are provided (collorative songs for example)
    artists = [artist.text for artist in now_playing_artist_section]
    current_artists = ', '.join(artists)
    current_song = re.search(now_playing_regex, now_playing_section)
    current_song = current_song.group(1)
    return current_song, current_artists



def chat(browser, last_fm_data, room):
    '''

    '''
    if browser.current_url != f'https://app.jqbx.fm/room/{room}':
        browser.get(f'https://app.jqbx.fm/room/{room}')
        time.sleep(5)
    current_song, current_artists = now_playing(browser)
    message, all_messages = message_tracker_with_whispers(browser)

    rock_tracker = []
    rock_out = False

    #keys in keywords dict correlate directly to a response in the responses dict.
    #this part is a work-in-progress and kind of clunky but it works. 
    #Users request something silly and I add the trigger/response to these two dictionaries.
    # each keyword has a list of strings that will trigger a resposne
    keywords = {'beer': ['/doopbot beer', 'doopbot beer me!!!'],
                'taxes': ['/doopbot taxes', 'doopbot do my taxes!!!'],
                'choose': ['/doopbot choose'],
                'DutchDon': ['/dd'],
                'Bumble': ['/bumble'],
                'LNL': ['/lnl'],
                'Proph': ['/proph'],
                'DD_party': ['/dd party'],
                'DD_motorboat': ['/dd motorboat'],
                'DD_ladies': ['/dd ladies'],
                'metallica': ['/metallica'],
                'spongebob1': ['are you ready kids?'],
                'spongebob2': ["i can't hear you!"],
                'pizza': ['pizza me'],
                'thanks': ['thanks doopbot'],
                'burto': ['/burto'],
                'sprucey': ['/sprucey'],
                'spined': ['/spined'],
                'roomdaddy': ['/room daddy'],
                'scirton': ['/scirton'],
                'dloc': ['/loc'],
                'joe': ['/joe'],
                'cpa': ['/cpa'],
                'tread': ['/tread'],
                'ddocktober': ['/dd oktoberfest'],
                'ddtrump': ['/dd trump'],
                'ddballoon': ['/dd balloon'],
                'ddsing': ['/dd sing'],
                'vinyls': ['vinyls'],
                'dddisco': ['/dd disco'],
                'ddbirds': ['/dd birds'],
                'ddsad': ['/dd sad'],
                'ddcovid': ['/dd covid'],
                'ddbeer': ['/dd beer'],
                'peace': ['/peace'],
                'bye': ['/bye'],
                'cya': ['/cya'],
                'help': ['/doopbot help'],
                'friyay': ['/friyay'],
                'sluts': ['/sluts'],
                'source_help': ['/source'],
                'edward-bootle': ['edward bootle'],
                'dengar': ['!dengar']}

    #probably unnecessary regex conversion to allow more flexible keyword matching.
    for key, value in keywords.items():
        regex_trigger = []
        for trigger in value:
            regex_trigger.append(f'.*({trigger})')
            if '/' in trigger:
                regex_trigger.append(f'.*({trigger.replace("/", "!")})')
        keywords[key] = re.compile('|'.join(regex_trigger))
    
    #anytime a user says doopbot and beer in the same sentence, doopbot will post a beer emoji. this overrides the earlier trigger...
    keywords['beer'] = re.compile(r'.*(beer|doopbot[^"/<]).*(beer|doopbot[^"/<])|/doopbot beer')
    keywords['coffee'] = re.compile(r'.*(coffee|doopbot[^"/<]).*(coffee|doopbot[^"/<])|/doopbot coffee')
    keywords['pizza'] = re.compile(r'.*(pizza|doopbot[^"/<]).*(pizza|doopbot[^"/<])|/doopbot pizza|pizza me')
    keywords['doopbot'] = re.compile(r'^/doopbot$|^!doopbot$')
    keywords['DutchDon'] = re.compile(r'^/dd$|^!dd$')
    responses = {'beer': [':beer:'],
                'coffee': [':coffee:'],
                'taxes': ["Just call @Section 59(e)2 and mention promo code PIZZA+BEER and he'll do them for free!"],
                'choose': [f'I choo choo chooose {choose()}'],
                'DutchDon': ['https://cdn.discordapp.com/attachments/816004497276141578/816004517652332641/image0.jpg'],
                'Bumble': ['https://media.giphy.com/media/nDMyoNRkCesJdZAuuL/giphy.gif'],
                'LNL': ['https://cdn.discordapp.com/attachments/816016377839353896/816017259961122816/Laurens-3.jpg'],
                'Proph': ['https://media1.tenor.com/images/6bf71c857a5c14ca9f94464d325a7684/tenor.gif'],
                'DD_party': ['https://cdn.discordapp.com/attachments/816004497276141578/816014984793948181/image0.gif'],
                'DD_motorboat': ['https://cdn.discordapp.com/attachments/816004497276141578/816728523221630977/image0.gif'],
                'DD_ladies': ['https://cdn.discordapp.com/attachments/816004497276141578/816722784385630288/image0.jpg'],
                'metallica': [':the_horns:Fuck Radiohead!:the_horns:'],
                'spongebob1': ['aye aye captain!!'],
                'spongebob2': ['AYE AYE CAPTAIN!!!'],
                'pizza': [':pizza:'],
                'thanks': ['no problem boss. pizza and beer for life!'],
                'burto': ['https://media.giphy.com/media/l0IymLhLT614b2gKY/giphy.gif'],
                'sprucey': ['https://media1.tenor.com/images/ceb69928e0986105273a3f8d9aec5d4f/tenor.gif'],
                'spined': ['https://static.wikia.nocookie.net/powerlisting/images/1/1d/Sub-zero-s-spine-rip-mk-9-o.gif'],
                'roomdaddy': ['https://media.giphy.com/media/l378vB87ICLy6Qbn2/giphy.gif'],
                'scirton': ['https://images-ext-1.discordapp.net/external/x0eXkCZx-yHuXIrgLn3QCHxIjqSId1sRo4nYg4YyaiI/%3Fitemid%3D10939320/https/media1.tenor.com/images/c2d42d55d52596ec65a8b282297191d1/tenor.gif'],
                'dloc': ['https://media.giphy.com/media/llejaeuDGbfws/giphy.gif', 'https://media.giphy.com/media/3oKIPs6VsHLpVXnZTy/giphy.gif'],
                'joe': ['https://media.giphy.com/media/hrFNAhUzCiw0BQB3KP/giphy.gif', 'https://media.giphy.com/media/uiycoFeG7pZ9jhiETJ/giphy.gif', 'https://media.giphy.com/media/LrGVCqbhMTXhI7THeT/giphy.gif', 'https://media.giphy.com/media/KFuEnVvWnRkhHM884J/giphy.gif', 'https://media.giphy.com/media/PhCXL27wF2rIu7crdf/giphy.gif', 'https://media.giphy.com/media/hQvHOUvgd5iZ6unsLE/giphy.gif'],
                'cpa': ['https://media.giphy.com/media/nuXIEASt1wqI/giphy.gif'],
                'tread': ['https://media.giphy.com/media/1Ag3zlcSgvZimcRjsy/giphy.gif'],
                'ddocktober': ['https://media.discordapp.net/attachments/816004497276141578/817466773816213504/image0.jpg'],
                'ddtrump': ['https://cdn.discordapp.com/attachments/816004497276141578/817470021302353982/image0.gif'],
                'ddballoon': ['https://cdn.discordapp.com/attachments/816004497276141578/817474937324437585/IMG_0623.JPG'],
                'dddisco': ['https://media.discordapp.net/attachments/816004497276141578/818613196579536946/image0.gif'],
                'ddsing': ['https://cdn.discordapp.com/attachments/816004497276141578/818609353053306930/image0.gif'],
                'vinyls': ["ummm. did you just spell 'vinyl'... With an 'S'? that is against the rules. once you've edited your comment we'll allow you back into the club."],
                'doopbot': ['https://media.giphy.com/media/10uTjZCI1pGVZS/giphy.gif'],
                'ddbirds': ['https://media.discordapp.net/attachments/816004497276141578/818927660889145415/image0.gif'],
                'ddsad': ['https://media.discordapp.net/attachments/816004497276141578/818949062433833030/image0.gif'],
                'ddcovid': ['https://media.discordapp.net/attachments/816004497276141578/818954062208696360/image0.gif'],
                'ddbeer': ['https://cdn.discordapp.com/attachments/816004497276141578/819252391827472394/image0.gif'],
                'peace': ['https://media.giphy.com/media/5nczOUmGibjzO/giphy.gif'],
                'bye': ['https://media.giphy.com/media/m9eG1qVjvN56H0MXt8/giphy.gif'],
                'cya': ['https://media.giphy.com/media/kaBU6pgv0OsPHz2yxy/giphy.gif'],
                'help': ['tinyurl.com/doopbot'],
                'friyay': ['https://media.giphy.com/media/1APaqOO5JHnWKLc7Bi/giphy.gif'],
                'sluts': ['https://media.giphy.com/media/YOnPSM1sO7lx6/giphy.gif', 'https://media.giphy.com/media/3o7TKWKHCUtXZXvj9K/giphy.gif'],
                'source_help': ['https://media.giphy.com/media/IyuvNRoH3OPrLEiIgH/giphy.gif'],
                'edward-bootle': ['https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/2nd_Lord_Skelmersdale_by_Ape.jpg/200px-2nd_Lord_Skelmersdale_by_Ape.jpg'],
                'dengar': ['https://media.giphy.com/media/9AIqHZsSBvZRf8U7y3/giphy.gif']}
    

    last_message = 'defaultvalue'
    last_results = {}
    nominees = []
    voting = False
    display_lastFM_stats = False
    song_logged = False
    now = datetime.now()
    theme = 'No theme selected. Run !theme or !qt to generate one.'
    theme_message = 'No theme selected. Run !theme or !qt to generate one.'
    queue = [{}]
    listeners = []
    miss_groupie_commands = ['album', 'help', 'artist', 'audiohelp', 'auto-artist', 'auto-best', 'auto-first', 'best', 'btc', 'choose', 'giphy','compliment', 'dadjoke', 'donate', 'down', 'fact', 'first', 'github', 'haha', 'hay', 'hay', 'inspire', 'joke', 'lastfm', 'me', 'no', 'queue', 'rare', 'relink', 'report', 'ro$', 'roll', 'roulette', 'skip', 'stats', 'time', 'top', 'tv', 'up', 'weather', 'whosampled', 'wiki', 'you-here', 'yt', 'afk', 'config', 'update', 'welcome']
    miss_g_regex = re.compile('|'.join([f'^{i}' for i in miss_groupie_commands]))
    for trigger in value:
        regex_trigger.append(f'.*({trigger})')
        if '/' in trigger:
            regex_trigger.append(f'.*({trigger.replace("/", "!")})')

    while True:
        responded = False
        progress = progress_bar(browser)

        four_twenty_reminder = fourtwenty_spin()
        if '420' in four_twenty_reminder:
            send_message(browser, four_twenty_reminder)
        #time.sleep(0.2)
        message, all_messages = message_tracker_with_whispers(browser)
        song, artists = now_playing(browser)
        if progress > 95 and song_logged == False:
            print('songs almost done')
            listeners = get_listeners(browser)
            queue = get_queue(browser)
            song_logged = True

        if song != current_song:
            '''
            This 'if' block runs as soon as a new song starts.

            Resets the /rock counter to 0.
            Scrapes the song/artist/DJ info.
            Checks the song+artist and counts the occurrences in the last.fm CSV file (jqbx_history.csv)
            Some silly keyword messages/gifs when a word appears in a song.
            If the song isn't in the csv file (listen_count == 0), doopbot will dope the track.
            '''
            #log last spin
            final_spin_stats = get_jqbx_history(browser, room, False)[0]
            if not queue[0].get('user'):
                queue = get_queue(browser)
            last_spin = {'user': queue[0]['user'],
                         'spotify_id': queue[0]['spotify_id'],
                         'song': current_song,
                         'artists': current_artists,
                         'starred_by_users': '~~~'.join([i['user'] for i in listeners if i['starred'] == True]),
                         'starred_by_id': '~~~'.join([i['spotify_id'] for i in listeners if i['starred'] == True]),
                         'total_stars': int(final_spin_stats[2]),
                         'total_dopes': int(final_spin_stats[0]),
                         'total_nopes': int(final_spin_stats[1]),
                         'time': now.strftime('%Y-%m-%d %H:%M:%S')}
            last_song_write(last_spin, room)
            now = datetime.now()
            current_song = song
            current_artists = artists
            song_logged = False
            print(f'{artists} {song}')
            rock_tracker = []
            rock_out = False


            if voting == True:
                poll_results = strawpoll_results(poll)
                print(poll_results)
                voters = [i['name'] for i in poll_results.json()['content']['poll']['voters']]
                send_message(browser, f"doopbot received votes from {', '.join(voters)}")
                send_message(browser, f"Total votes received --- {poll_results.json()['content']['poll']['total_votes']}")
                print(f"{poll_results.json()['content']['poll']['voters']}")
                for i in poll_results.json()['content']['poll']['poll_answers']:
                    send_message(browser, f"{i['answer']} received {i['votes']}")
                voting = False
                nominees = []
                    
            listen_count = 0

            for track in last_fm_data:
                if f'{track[2].lower()} {track[6].lower()}' == f'{artists.lower()} {song.lower()}':
                    listen_count += 1

            #Harrass the DJ if they're one of our friends in the admin list and they play a song we've heard already. Currently disabled.
            if display_lastFM_stats == True:
                if listen_count > 0:
                    send_message(browser, f"Nice spin {queue[0]['user']} I've heard {song} by {artists} {listen_count} times.")
                if listen_count == 0:
                    send_message(browser, f"I don't think I've heard {song} by {artists} in this room before. Nice job {queue[0]['user']}")

            
            if song.lower() == 'horn intro':
                #This 9 second song is played as a joke. Doopbot disapproves.
                nope_button(browser)

            if listen_count == 0:
                #doopbot likes the current song if it hasn't been played in the room before. Hooray for bonus internet points.
                dope_button(browser)


        if message != last_message:
            last_message = message
            print(f"new message from {message['user']}: {message['text']}")

            if message['text'].lower()[:1] == '!': #or message['text'].lower()[:1] == '/':

                command = message['text'].lower()[1:]

                if command == 'help' or command == '?':
                    send_message(browser, f"/w @{message['user']} tinyurl.com/doopbot")
                elif command[:9] == 'starboard':
                    if re.search(r'[0-9]', command):
                        hours = int(re.sub(r'[^0-9]', '', command))
                        if hours <= 24:
                            high_score(browser, hours)
                    else: 
                        high_score(browser, 10)
                elif command == 'yt':
                    rick_roll_random = random.randint(1,10)
                    if rick_roll_random == 10:
                        send_message(browser, f"youtube.com/watch?v=dQw4w9WgXcQ")
                    else:
                        youtube_search(current_song, current_artists, browser)
                elif command[:5] == 'theme' or command[:2] == 'qt':
                    if 'insane' in command or command == 'qti':
                        theme_message, theme = insane_theme()
                    else:
                        theme_message, theme = get_theme()
                    send_message(browser, f"{theme_message}")

                elif command == 'current' or command == 'ct':
                    send_message(browser, f"{theme_message}")

                elif command[:6] == 'choose':
                    options = command[6:].strip().split(',')
                    if len(options) == 1:
                        options = [':beer:', ':pizza:']
                        send_message(browser, f"I choo choo choose {random.choice(options)}")
                    else:
                        send_message(browser, f"I choo choo choose... {random.choice(options)}")

                elif command == 'rock':
                    if message['user'] not in rock_tracker:
                        rock_tracker.append(message['user'])
                        print(rock_tracker)
                    if len(rock_tracker) == 3 and rock_out == False:
                        rock_out = True
                        send_message(browser, f":beer:You played {song}?! HOW DID YOU KNOW?! THIS IS MY FAVORITE SONG!!! ROCK ON!!!:the_horns::the_horns:")
                        send_message(browser, "https://media.giphy.com/media/tDFfAG1vTHfGw/giphy.gif")
                        star_button(browser)

                elif command == 'slots':
                    slots(message, browser)

                elif command == 'room-firsts' or command == 'rf':
                    display_lastFM_stats = not display_lastFM_stats
                    if display_lastFM_stats:
                        send_message(browser, f"last fm room stats toggled on")
                    else:
                        send_message(browser, f"last fm room stats toggled off")

                elif command == 'pbstats':
                    send_message(browser, f"I've heard {song} by {artists} {listen_count} times in the Pizza/Beer room.")

                elif command == 'queuecount' or command == '#':
                    count = len(get_queue(browser))
                    listener_count = int(browser.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/div[3]/div[2]/ul[1]/li[1]/a/em').text)
                    percent = count/listener_count
                    send_message(browser, f"{count} DJs in queue. ({percent:.2%} of room)")
                elif command[:11] == 'battle-vote' or command[:2] == 'bv':
                    if re.search(r'[0-9]', command):
                        number_of_entries = int(re.sub(r'[^0-9]', '', command))
                        if number_of_entries <= 30:
                            voting = True
                            results = {}
                            history = get_jqbx_history(browser, room, False)
                            nominees =[f'{i[5].strip()} -- {i[3].strip()} -- {i[4].strip()}' for i in history[:number_of_entries]]
                            poll = strawpoll_create(nominees, f"BATTLE!!! {theme}")
                            send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')
                        else:
                            send_message(browser, f"I don't wanna. I think strawpoll limits to 30 entries anyway.")
                        
                    else:
                        send_message(browser, f'Must provide a number after the battle-vote/bv command.')
                elif command == 'vote-reset':
                    voting = False
                    nominees = []
                elif command == 'vote-reminder' or command == 'vote!':
                    if len(nominees) > 0:
                        send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')
                        send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')
                        send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')
                        send_message(browser, f'VOTE!!! https://strawpoll.com/{poll}')


                else:
                    for action, trigger in keywords.items():
                        if re.search(trigger, message['text'].lower()) and '9lmdbr25slw401t2mmq86y2sd' not in message['spotify_id']:
                            if 'bdygert5150' in message['spotify_id'] and action == 'beer':
                                #silly one-off rule for a JQBX user that enjoys seeing multiple beer emoji.
                                send_message(browser, ':beer::beer:')
                                responded = True
                                continue
                            send_message(browser, random.choice(responses[action]))
                            responded = True
                    if responded == False and '!' in message['text'].lower()[:1]:
                        send_message(browser, f"/w @{message['user']} umm was I supposed to do something with that? Check out some possible commands at tinyurl.com/doopbot")
#                if '/' in message['text'].lower()[:1] and not re.search(miss_g_regex, command):
#                    send_message(browser, f"/w @{message['user']} I'm going to phase out the / prefix eventually. Try out ! next time. tinyurl.com/doopbot :kissing_heart:")


            else:
                for action, trigger in keywords.items():
                    if re.search(trigger, message['text'].lower()) and '9lmdbr25slw401t2mmq86y2sd' not in message['spotify_id']:
                        if 'bdygert5150' in message['spotify_id'] and action == 'beer':
                            #silly one-off rule for a JQBX user that enjoys seeing multiple beer emoji.
                            send_message(browser, ':beer::beer:')
                            responded = True
                            continue
                        send_message(browser, random.choice(responses[action]))
                        responded = True
            if message['text'].lower() == 'wut':
                if 'span title' not in all_messages[-2]:
                    send_message(browser, f"You didn't hear?  I'll say it louder for you...They said...")
                    send_message(browser, f"{all_messages[-2].upper()}!!!!!!!!!!!")
                else:
                    send_message(browser, f"huh. I wasn't listening either.")






def insane_theme():
    req = requests.get('https://en.wikipedia.org/wiki/Special:Random')
    soup = bs4.BeautifulSoup(req.content)
    return f":pizza: THEME -- {soup.select('#firstHeading')[0].text.upper()} :beer:", soup.select('#firstHeading')[0].text.upper()

def baby_theme():
    baby_themes = ['Rock and Roll', 'Coloring', 'Songs that mention the word "Baby"', 'Crawling', 'Friendship', 'Playground', 'Mom and Dad', 'Lullaby', 'Weather', 'Happy', 'Sad'  ]
    theme = random.choice(baby_themes)
    return f":pizza: {soup.select('#firstHeading')[0].text.upper()} :beer:", soup.select('#firstHeading')[0].text.upper()

def get_theme():
    '''
    Chooses a random theme from the room's theme list
    I downloaded a copy of the google sheets theme list and put them in a text file.
    returns a string that can be posted to the chat that indicates the current theme. 
    '''
    with open('jqbx_themes.txt') as file:
        themes = file.readlines()
    random_theme = random.randint(0,len(themes))
    return f':pizza: THEME #{random_theme} --{themes[random_theme].upper()} :beer:', themes[random_theme].upper()

def choose():
    '''
    A pizza/beer themed coin-flip.
    '''
    choice = random.randint(1, 2)
    if choice == 1:
        return ':beer:'
    if choice == 2:
        return ':pizza:'

def send_message(browser, message):
    '''
    Types into the chat box. Clicks send.
    '''
    browser.find_element_by_css_selector('#chat-input-form > div:nth-child(1) > input[type=text]').send_keys(message)
    browser.find_element_by_css_selector('#chat-input-form > button').click()

def dope_button(browser):
    '''
    Clicks the dope/thumbs-up button for the current song.
    '''
    browser.find_element_by_css_selector('div.meter:nth-child(1) > span:nth-child(1) > button:nth-child(2) > span:nth-child(1)').click()

def nope_button(browser):
    '''
    Boooooooo! Clicks the nope button for the current song.
    '''
    browser.find_element_by_css_selector('div.meter:nth-child(1) > span:nth-child(1) > button:nth-child(1) > span:nth-child(1)').click()

def star_button(browser):
    '''
    Clicks the star button. JQBX's most coveted award!
    '''
    browser.find_element_by_css_selector('div.music:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(3) > span:nth-child(1) > button:nth-child(2)').click()



def get_jqbx_history(browser, room, full_history):
    '''
    Checks the room's current 100 track history.
    '''
    if browser.current_url != f'https://app.jqbx.fm/room/{room}':
        browser.get(f'https://app.jqbx.fm/room/{room}')
        time.sleep(5)
    browser.find_element_by_css_selector('#root > div > div > div > div.container > div:nth-child(3) > div.tabs > ul.labels > li:nth-child(2)').click() #clicks history tab.
    #Set the browser window to a really tall value so that all the dynamically loaded content appears on the page
    #Otherwise I'd have to emulate scrolling down the history
    if full_history:
        browser.set_window_size(1024, 8000)
        time.sleep(12)
    history_content = browser.find_element_by_css_selector('#root > div > div > div > div.container > div:nth-child(3) > div.tabs > ul.content > li.active > div > div').get_attribute('innerHTML')
    if full_history:
        browser.set_window_size(1024, 700)

    soup = bs4.BeautifulSoup(history_content)
    new_regex = re.compile(r'<li class="thumbs-up">(\d+)</li><li class="thumbs-down">(\d+)</li><li class="stars">(\d+)</li></ul>.*?title.*?name"\stitle="(.*?)".*?class="artists"\stitle="(.*?)".+?spotify:user.+?>(.*?)<')
    parsed_history = re.findall(new_regex, str(soup))

    return parsed_history



def build_spotify_playlist(room, jqbx_history):
    '''
    This was the original function for 'doopbot'
    take the parsed 100 track history found on the JQBX room.
    Add anything that was awarded > 1 or 2 stars by JQBX users
    to a Spotify Playlist (my attempt at a crowd-sourced 'best-of' playlist) 
    '''
    SPOTIFY_ID = '55e80011675c4021aab2a189cd360405'
    SPOTIFY_SECRET = os.environ.get('SPOTIFY_APP_KEY')
    spotify_username = 'boncora'
    scope = 'playlist-modify-private, user-read-recently-played, playlist-modify-public, playlist-read-private, user-follow-read, user-read-private'
    token = util.prompt_for_user_token(username=spotify_username, scope=scope, client_id=SPOTIFY_ID,
                                       client_secret=SPOTIFY_SECRET, redirect_uri="http://localhost:8080/callback")
    sp = spotipy.Spotify(auth=token)
    spotify_playlist_1 = []
    spotify_playlist_2 = []
    new_stars_1 = []
    new_stars_2 = []
    tracks_not_found = []
    for song in jqbx_history:
        if int(song[2]) > 1:
            removechars = re.compile(r"[&']|amp;")
            artist = removechars.sub("", song[4])
            title = removechars.sub("", song[3])
            search_results = sp.search(q=f"artist:{artist} AND track:{title}", type='track', market='from_token')
            if len(search_results['tracks']['items']) > 0:
                uri = search_results['tracks']['items'][0]['uri']
                spotify_playlist_1.append(uri)
            else:
                tracks_not_found.append(f"{artist}--{title}")
        if int(song[2]) > 2:
            removechars = re.compile(r"[&']|amp;")
            artist = removechars.sub("", song[4])
            title = removechars.sub("", song[3])
            search_results = sp.search(q=f"artist:{artist} AND track:{title}", type='track', market='from_token')
            if len(search_results['tracks']['items']) > 0:
                uri = search_results['tracks']['items'][0]['uri']
                spotify_playlist_2.append(uri)
            else:
                tracks_not_found.append(f"{artist}--{title}")
    if room == 'rock':
        with open("C:\Cory\Python\pb_jqbx_stars.txt", "a+") as myfile:
            myfile.seek(0)
            file = myfile.read()
            for track in spotify_playlist_1:
                if track in file:
                    continue
                else:
                    myfile.write(track + '\n')
                    new_stars_1.append(track)
        if len(new_stars_1) > 0:
            sp.user_playlist_add_tracks('boncora', 'spotify:playlist:4sXChbEoPWTiyKf0lOLPj2', new_stars_1, position=0)
        with open("C:\Cory\Python\pb_jqbx_megastars.txt", "a+") as myfile:
            myfile.seek(0)
            file = myfile.read()
            for track in spotify_playlist_2:
                if track in file:
                    continue
                else:
                    myfile.write(track + '\n')
                    new_stars_2.append(track)
        if len(new_stars_2) > 0:
            sp.user_playlist_add_tracks('boncora', 'spotify:playlist:5c8SYbTytW5Ly9OJX4Oxkc', new_stars_2, position=0)
    elif room == 'chillvibes':
        with open("C:\Cory\\Python\chillvibes_jqbx_stars.txt", "a+") as myfile:
            myfile.seek(0)
            file = myfile.read()
            for track in spotify_playlist_1:
                if track in file:
                    continue
                else:
                    myfile.write(track + '\n')
                    new_stars_1.append(track)
        if len(new_stars_1) > 0:
            sp.user_playlist_add_tracks('boncora', 'spotify:playlist:3wQmoKLqfn2RuHyz4pOy9o', new_stars_1, position=0)
        with open("C:\Cory\Python\chillvibes_jqbx_megastars.txt", "a+") as myfile:
            myfile.seek(0)
            file = myfile.read()
            for track in spotify_playlist_2:
                if track in file:
                    continue
                else:
                    myfile.write(track + '\n')
                    new_stars_2.append(track)
        if len(new_stars_2) > 0:
            sp.user_playlist_add_tracks('boncora', 'spotify:playlist:0nX7H7EWMVmaiZw9ml2tS0', new_stars_2, position=0)
    elif room == 'hiphopheads':
        with open("C:\Cory\\Python\hiphop_jqbx_stars.txt", "a+") as myfile:
            myfile.seek(0)
            file = myfile.read()
            for track in spotify_playlist_1:
                if track in file:
                    continue
                else:
                    myfile.write(track + '\n')
                    new_stars_2.append(track)
        if len(new_stars_2) > 0:
            sp.user_playlist_add_tracks('boncora', 'spotify:playlist:2znkmccNDdAjv3l5Iix7PT', new_stars_2, position=0)

    for i in tracks_not_found:
        print('Could not find ' + i)
    
def lastfm_csv():
    '''
    I downloaded a csv of the rooms last.fm listening history (https://www.last.fm/user/JQBX-Pizza-Beer).
    csv export tool (https://lastfm.ghan.nl/export/)
    returns a list of all the songs that have been played in the room.
    '''
    open_file = open('jqbx_history.csv', encoding = 'cp850')
    reader = csv.reader(open_file)
    data = list(reader)
    return data


def main():
    rooms = ['rock', 'hiphopheads', 'chillvibes']
    #room = 'rock'
    room = 'rock'
    browser = jqbx_login(room)
    #room = '4d3d3d3'
    #for room in rooms:
    #    jqbx_history = get_jqbx_history(browser, room, True)
    #    build_spotify_playlist(room, jqbx_history)
    last_fm_history = lastfm_csv()
    while True:
        try:
            chat(browser, last_fm_history, room)
        except NoSuchElementException:
            print('could not find element')
            time.sleep(5)
            continue
        except StaleElementReferenceException:
            print('stale reference')
            time.sleep(5)
            continue
        except Exception:
            print('could not find element')
            time.sleep(5)
            continue

try:
    main()
except Exception as e:
    print(f'cannot connect! { e }')
